ios-samples
===========

iOS勉強にて作ったサンプル


NSMutableDictionaryにsetする際のkeyはretainされるのか？
----
結果：keyがまだ無ければ「所有される」し（retainCount+1)、すでにkeyがあれば「所有されない」

[テストコード](https://github.com/taktamur/ios-samples/blob/master/RetainCountTest/RetainCountTest/AppDelegate.m)
