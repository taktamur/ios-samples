//
//  main.m
//  NoMapNavi
//
//  Created by 田村 孝文 on 2012/12/23.
//  Copyright (c) 2012年 田村 孝文. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "NMNAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([NMNAppDelegate class]));
    }
}
