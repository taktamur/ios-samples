//
//  NMNCenterPinAnnnotation.h
//  NoMapNavi
//
//  Created by 田村 孝文 on 2012/12/23.
//  Copyright (c) 2012年 田村 孝文. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <YMapKit/YMapKit.h>
#import <CoreLocation/CoreLocation.h>

@interface NMNCenterPinAnnnotation : NSObject<YMKAnnotation>
@property(nonatomic) CLLocationCoordinate2D coordinate;

-(NSString *)title;
-(NSString *)subtitle;
@end
