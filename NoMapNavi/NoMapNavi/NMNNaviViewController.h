//
//  NMNNaviViewController.h
//  NoMapNavi
//
//  Created by 田村 孝文 on 2012/12/24.
//  Copyright (c) 2012年 田村 孝文. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
@interface NMNNaviViewController : UIViewController<CLLocationManagerDelegate>
@property(nonatomic)CLLocation *targetLocation;
@end
