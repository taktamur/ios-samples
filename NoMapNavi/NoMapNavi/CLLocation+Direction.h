//
//  CLLocation+Direction.h
//  NoMapNavi
//
//  Created by 田村 孝文 on 2012/12/23.
//  Copyright (c) 2012年 田村 孝文. All rights reserved.
//

#import <CoreLocation/CoreLocation.h>

@interface CLLocation (Direction)
- (CLLocationDirection)aboutAzimuthToLocation:(const CLLocation *)target;
@end
