//
//  NMNViewController.h
//  NoMapNavi
//
//  Created by 田村 孝文 on 2012/12/23.
//  Copyright (c) 2012年 田村 孝文. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import <YMapKit/YMapKit.h>

@interface NMNViewController : UIViewController<CLLocationManagerDelegate,YMKMapViewDelegate>

@end
