//
//  NMNViewController.m
//  NoMapNavi
//
//  Created by 田村 孝文 on 2012/12/23.
//  Copyright (c) 2012年 田村 孝文. All rights reserved.
//

#import "NMNViewController.h"
#import "NMNCenterPinAnnnotation.h"
#import <YMapKit/YMapKit.h>
#import "CLLocation+Direction.h"
#import <AudioToolbox/AudioServices.h>
#import "NMNNaviViewController.h"

@interface NMNViewController ()
@property (nonatomic) YMKMapView *mapView;
@property(nonatomic)YMKRouteOverlay *routeOrverlay;
@property(nonatomic)NMNCenterPinAnnnotation *targetPin;
@property(nonatomic)CLLocation *location;

@property(nonatomic)CLLocationDirection heading;

@property(nonatomic)NSDate *lastVibrateTime;
@end

@implementation NMNViewController
@synthesize mapView,routeOrverlay,lastVibrateTime;

static NSString *myappid = @"iqepABuxg66Ex7hDBiYGDXZQkg_NSSY.EsBVsKX27f3_Sd2814W4gy_QmARcGg--";

#pragma mark - UIViewController
- (void)viewDidLoad
{
    [super viewDidLoad];

    // 地図の初期化
    self.mapView =  [[YMKMapView alloc] initWithFrame:CGRectMake(0, 0, 320, 320) appid:myappid ];
    self.mapView.mapType = YMKMapTypeStandard;
    self.mapView.delegate=self;
    self.mapView.showsUserLocation=YES;
    [self.view addSubview:self.mapView];
    

    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"ToNavi"]) {
        NMNNaviViewController *naviController = (NMNNaviViewController *)[segue destinationViewController];

        CLLocation *target = [[CLLocation alloc]initWithLatitude:self.mapView.centerCoordinate.latitude
                                                       longitude:self.mapView.centerCoordinate.longitude];
        naviController.targetLocation = target;
    }
}



#pragma mark - YMKMapDelegate
-(void)mapView:(YMKMapView *)map regionDidChangeAnimated:(BOOL)animated
{
    if( self.targetPin!=nil){
        // 中心ピンを移動
        NSArray *annotaions = map.annotations;
        [map removeAnnotations:annotaions];

        self.targetPin.coordinate = map.centerCoordinate;
        [map addAnnotation:self.targetPin];
    }
}
-(void)mapView:(YMKMapView *)mapView didUpdateUserLocation:(YMKUserLocation *)userLocation
{
    if(self.targetPin == nil ){
        self.targetPin = [[NMNCenterPinAnnnotation alloc]init];
        self.targetPin.coordinate = userLocation.coordinate;
        self.mapView.region = YMKCoordinateRegionMake(self.targetPin.coordinate,
                                                      YMKCoordinateSpanMake(0.002, 0.002));

    }
}


@end


