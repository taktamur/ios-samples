//
//  CLLocation+Direction.m
//  NoMapNavi
//
//  Created by 田村 孝文 on 2012/12/23.
//  Copyright (c) 2012年 田村 孝文. All rights reserved.
//


#import "CLLocation+Direction.h"


@implementation CLLocation (Direction)
- (CLLocationDirection)aboutAzimuthToLocation:(const CLLocation *)target
{
    d = d<0.0 ? d+360.0 : d;
    return d;
}
@end
