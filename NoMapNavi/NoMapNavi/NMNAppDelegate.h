//
//  NMNAppDelegate.h
//  NoMapNavi
//
//  Created by 田村 孝文 on 2012/12/23.
//  Copyright (c) 2012年 田村 孝文. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NMNAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
