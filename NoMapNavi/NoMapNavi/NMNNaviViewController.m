//
//  NMNNaviViewController.m
//  NoMapNavi
//
//  Created by 田村 孝文 on 2012/12/24.
//  Copyright (c) 2012年 田村 孝文. All rights reserved.
//

#import "NMNNaviViewController.h"
#import "CLLocation+Direction.h"
#import <AudioToolbox/AudioServices.h>

@interface NMNNaviViewController ()
@property (weak, nonatomic) IBOutlet UITextView *messageView;
@property (weak, nonatomic) IBOutlet UILabel *distanceLabel;
@property (weak, nonatomic) IBOutlet UILabel *allowLabel;
@property (nonatomic)CLLocationManager *locationManager;
@property(nonatomic)CLLocation *nowLocation;
@property(nonatomic)CLLocationDirection heading;
@property(readonly)double kakudo;
@property(nonatomic)NSDate *lastVibrateTime;

@end

@implementation NMNNaviViewController
@synthesize targetLocation,locationManager,nowLocation,heading,lastVibrateTime;

#pragma mark - UIViewController
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    // locationManagerの初期化
    self.locationManager = [[CLLocationManager alloc] init];
    
    // 位置情報サービスが利用できるかどうかをチェック
    if ([CLLocationManager locationServicesEnabled]) {
        self.locationManager.delegate = self;
        
        // 測位開始
        self.locationManager.headingFilter=10.0;
    } else {
        NSLog(@"Location services not available.");
    }
    self.lastVibrateTime=[NSDate date];
    
    [UIDevice currentDevice].proximityMonitoringEnabled=YES;
}

-(void)viewDidAppear:(BOOL)animated
{
    [self updateMessageView];
    [self.locationManager startUpdatingHeading];
    [self.locationManager startUpdatingLocation];
    [UIDevice currentDevice].proximityMonitoringEnabled=YES;
}

-(void)viewWillDisappear:(BOOL)animated
{
    [self.locationManager stopUpdatingHeading];
    [self.locationManager stopUpdatingLocation];
    [UIDevice currentDevice].proximityMonitoringEnabled=NO;
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - CLLocationManagerDelegate
// 位置情報更新時
- (void)locationManager:(CLLocationManager *)manager
    didUpdateToLocation:(CLLocation *)newLocation
           fromLocation:(CLLocation *)oldLocation {
    self.nowLocation=newLocation;
    [self updateMessageView];

    /*
    //緯度・経度を出力
    NSLog(@"didUpdateToLocation latitude=%f, longitude=%f",
          [newLocation coordinate].latitude,
          [newLocation coordinate].longitude);
    self.location = newLocation;
    
    if( self.targetPin == nil ){
        self.targetPin = [[NMNCenterPinAnnnotation alloc]init];
        self.targetPin.coordinate = newLocation.coordinate;
        [self.mapView addAnnotation:self.targetPin];
    }
     */
}
- (void)locationManager:(CLLocationManager *)manager didUpdateHeading:(CLHeading *)newHeading
{
    NSLog(@"heading=%f",newHeading.trueHeading);
    self.heading= newHeading.trueHeading;
    [self updateMessageView];
}
- (BOOL)locationManagerShouldDisplayHeadingCalibration:(CLLocationManager *)manager
{
    return YES;
}

#pragma mark - NMNNaviViewController
-(void)updateMessageView
{
    self.messageView.text=[self description];
    
    // 距離表示
    CLLocationDistance distance = [self.nowLocation distanceFromLocation:self.targetLocation];
    self.distanceLabel.text = [NSString stringWithFormat:@"あと%dm",
                               (int)distance];
    
    //ポケットに入れ、かつ「移動中」のときには、バイブでの「間違い」を連絡
    if( [UIDevice currentDevice].proximityState &&
       self.nowLocation.speed > 0.3 &&  //0.3mps以上を移動中とした。
       self.nowLocation.course > 0.0  &&  //courseがマイナスの場合は値がとれてない
       distance > 10.0 ){ // 10m以下になったら鳴らさない
        // バイブを鳴らす
        NSTimeInterval interval = [[NSDate date] timeIntervalSinceDate:self.lastVibrateTime];
        NSLog( @"interval=%f(sec)",interval);
        // n秒以上の間を置く、また
        if( interval > 2.0 ){
            self.lastVibrateTime = [NSDate date];
            if( 90.0 < self.kakudo && self.kakudo < 270.0 ){
                AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
            }
        }
    }
    
    // ⇧の回転
    self.allowLabel.transform = CGAffineTransformMakeRotation((CGFloat) (M_PI * self.headingkakudo / 180.0));
}

-(double)kakudo
{
    double kakudo =[self.nowLocation aboutAzimuthToLocation:targetLocation]-self.nowLocation.course;
    kakudo = kakudo<0 ? kakudo+360 : kakudo;
    return kakudo;
}
-(double)headingkakudo
{
    double kakudo =[self.nowLocation aboutAzimuthToLocation:targetLocation]-self.heading;
    kakudo = kakudo<0 ? kakudo+360 : kakudo;
    return kakudo;
}
#pragma mark - NSObject
-(NSString *)description
{
    NSString *desc = [NSString stringWithFormat:@"targetLocation=%@\n nowLocation=%@\n heading=%f\n kakudo=%f",
                      self.targetLocation,
                      self.nowLocation,
                      self.heading,
                      self.kakudo];
    return desc;
}

@end
