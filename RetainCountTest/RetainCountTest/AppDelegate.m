//
//  AppDelegate.m
//  RetainCountTest
//
//  Created by takafumi tamura on 12/10/03.
//  Copyright (c) 2012年 Takafumi Tamura. All rights reserved.
//

#import "AppDelegate.h"

@implementation NSNumber(test)
// NSStringのdescriptionにretainCountを付けようとしたら無限ループになったので、
// NSNumberを使う。
-(NSString *)description
{
    return [NSString stringWithFormat:@"%@ retainCount=%d",self, [self retainCount]];
}

@end



@implementation AppDelegate

- (void)dealloc
{
    [_window release];
    [super dealloc];
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]] autorelease];
    // Override point for customization after application launch.
    self.window.backgroundColor = [UIColor whiteColor];
    [self.window makeKeyAndVisible];
    
    //===========
    // RetainCountのテスト
    //  NSMutableDictionaryに入れるvalueは、内部でretainされるけど、
    //  keyはどうだっただろうか？の確認
    //===========
    NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
    {
        NSNumber *key1 = [[NSNumber alloc]initWithInt:100];
        NSNumber *value1 = [[NSNumber alloc]initWithInt:200];
        NSLog(@"%@",[key1 description]);
        NSLog(@"%@",[value1 description]);
        // ここでは両方retainCountは1
        
        // NSMutableDictionaryに入れてみる
        [dic setObject:value1 forKey:key1];
        NSLog(@"%@",[key1 description]);
        NSLog(@"%@",[value1 description]);
        // ここで両方retainCountは2 なので、keyもvalueもdicに「所有」された。
        
        [key1 release]; key1=nil;
        [value1 release]; value1=nil;
    }
    // 同じ値を持つ別オブジェクトにkeyを設定したときはどうなるか？
    {
        NSNumber *key2 = [[NSNumber alloc]initWithInt:100];
        NSNumber *value2 = [[NSNumber alloc]initWithInt:500];
        [dic setObject:value2 forKey:key2];
        NSLog(@"%@",[key2 description]); // retainCount=1 なので、すでにあるkeyと同じ値だと「所有」されない。
        NSLog(@"%@",[value2 description]);
        [key2 release]; key2=nil;
        [value2 release]; value2=nil;
    }
    // KVCの場合はどうなるか？
    {
        NSString *key3 = [[NSString alloc]initWithFormat:@"200"];
        NSNumber *value3 = [[NSNumber alloc]initWithInt:700];
        NSLog(@"%@ retainCount=%d",key3,[key3 retainCount]);
        NSLog(@"%@",[value3 description]);
        [dic setValue:value3 forKey:key3]; // KVCの場合、keyは文字列のみとなる。
        NSLog(@"%@ retainCount=%d",key3,[key3 retainCount]);
        NSLog(@"%@",[value3 description]);
        // ここで両方retainCountは2 なので、keyもvalueもdicに「所有」された。
        [key3 release];key3=nil;
        [value3 release];value3=nil;
    }
    [dic release];dic=nil;
    // 結論：setObject:forKey: も、setValue:forKey: も、
    // まだkeyが存在してなかった場合は「所有」されるし、
    // すでにkeyが存在している場合は「所有」されない。
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
