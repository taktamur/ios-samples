//
//  AppDelegate.h
//  RetainCountTest
//
//  Created by takafumi tamura on 12/10/03.
//  Copyright (c) 2012年 Takafumi Tamura. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
