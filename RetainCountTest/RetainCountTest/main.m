//
//  main.m
//  RetainCountTest
//
//  Created by takafumi tamura on 12/10/03.
//  Copyright (c) 2012年 Takafumi Tamura. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
