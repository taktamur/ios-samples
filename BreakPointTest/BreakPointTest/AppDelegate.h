//
//  AppDelegate.h
//  BreakPointTest
//
//  Created by 田村 孝文 on 2013/02/10.
//  Copyright (c) 2013年 田村 孝文. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
