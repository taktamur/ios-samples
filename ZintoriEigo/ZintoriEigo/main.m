//
//  main.m
//  ZintoriEigo
//
//  Created by 田村 孝文 on 2013/01/20.
//  Copyright (c) 2013年 田村 孝文. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ZEAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([ZEAppDelegate class]));
    }
}
