//
//  TableAndToolbarAppDelegate.m
//  TableAndToolbar
//
//  Created by tak on 09/03/22.
//  Copyright __MyCompanyName__ 2009. All rights reserved.
//

#import "TableAndToolbarAppDelegate.h"


@implementation TableAndToolbarAppDelegate

@synthesize window;
@synthesize navigationController;


- (void)applicationDidFinishLaunching:(UIApplication *)application {
	
	// Configure and show the window
	[window addSubview:[navigationController view]];
	[window makeKeyAndVisible];
}


- (void)applicationWillTerminate:(UIApplication *)application {
	// Save data if appropriate
}


- (void)dealloc {
	[navigationController release];
	[window release];
	[super dealloc];
}

@end
