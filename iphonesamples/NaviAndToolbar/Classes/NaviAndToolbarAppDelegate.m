//
//  NaviAndToolbarAppDelegate.m
//  NaviAndToolbar
//
//  Created by tak on 09/03/24.
//  Copyright __MyCompanyName__ 2009. All rights reserved.
//

#import "NaviAndToolbarAppDelegate.h"
#import "RootViewController.h"


@implementation NaviAndToolbarAppDelegate

@synthesize window;
@synthesize navigationController;
@synthesize toolBar;

- (void)applicationDidFinishLaunching:(UIApplication *)application {
	
	// Configure and show the window
	[window addSubview:[navigationController view]];
	[window makeKeyAndVisible];
	
	if( toolBar!=nil ){
		// 640-20(ステータス)-44(ナビゲーション)-44(ツールバー)=
		[toolBar setFrame:CGRectMake(0.0, 436.0, 320.0, 44.0)];
		[window addSubview:toolBar];
	}
}


- (void)applicationWillTerminate:(UIApplication *)application {
	// Save data if appropriate
}


- (void)dealloc {
	[navigationController release];
	[window release];
	[super dealloc];
}

@end
