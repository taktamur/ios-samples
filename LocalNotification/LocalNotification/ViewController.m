//
//  ViewController.m
//  LocalNotification
//
//  Created by Takafumi Tamura on 2013/02/24.
//  Copyright (c) 2013年 Takafumi Tamura. All rights reserved.
//

#import "ViewController.h"


@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - IBAction(Button)
- (IBAction)startNofiticationJustNow:(id)sender {
    DDLogWarn(@"%s", __func__);
    
}
- (IBAction)startNotificationAfter3min:(id)sender {
    DDLogWarn(@"%s", __func__);
    // http://adotout.sakura.ne.jp/?p=1058
    // 通知を作成する
    UILocalNotification *notification = [[UILocalNotification alloc] init];
    
    notification.fireDate = [[NSDate date] addTimeInterval:60 * 3]; // 10分後に通知
    notification.timeZone = [NSTimeZone defaultTimeZone];
    notification.alertBody = @"Notification3min";
    notification.alertAction = @"Open";
    notification.soundName = UILocalNotificationDefaultSoundName;
    
    // 通知を登録する
    [[UIApplication sharedApplication] scheduleLocalNotification:notification];
    
}
- (IBAction)startNotificationEvery1min:(id)sender {
    DDLogWarn(@"%s", __func__);
    // http://adotout.sakura.ne.jp/?p=1058
    // 通知を作成する
    UILocalNotification *notification = [[UILocalNotification alloc] init];
    
    notification.timeZone = [NSTimeZone defaultTimeZone];
    notification.alertBody = @"NotificationEvery1min";
    notification.alertAction = @"Open";
    notification.repeatInterval=NSMinuteCalendarUnit;
    notification.soundName = UILocalNotificationDefaultSoundName;
    
    // 通知を登録する
    [[UIApplication sharedApplication] scheduleLocalNotification:notification];
}
- (IBAction)stopNotification:(id)sender {
    DDLogWarn(@"%s", __func__);
    for(UILocalNotification *notification in [[UIApplication sharedApplication] scheduledLocalNotifications]) {
            [[UIApplication sharedApplication] cancelLocalNotification:notification];
    }
}

@end
