//
//  main.m
//  Way
//
//  Created by takafumi tamura on 12/08/05.
//  Copyright (c) 2012年 Takafumi Tamura. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "WAYAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([WAYAppDelegate class]));
    }
}
