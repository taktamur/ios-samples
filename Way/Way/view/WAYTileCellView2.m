//
//  WAYTileCellView2.m
//  Way
//
//  Created by takafumi tamura on 12/08/05.
//  Copyright (c) 2012年 Takafumi Tamura. All rights reserved.
//

#import "WAYTileCellView2.h"
#import "UIImageView+AFNetworking.h"

@interface WAYTileCellView2(){
    void (^_touched)(id sender);
}
@end

@implementation WAYTileCellView2
@synthesize uid=_uid;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.contentMode=UIViewContentModeScaleAspectFill;
        self.clipsToBounds=YES;
        self.userInteractionEnabled=YES;
    }
    return self;
}

-(void)setImageURL:(NSURL *)url
{
    [self setImageWithURL:url];
}

-(void)setTouched:(void (^)(id sender))touched
{
    _touched = touched;
}


-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    _touched(self);
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
