//
//  TileCellVIew.m
//  exp_TileView
//
//  タイル状の「画像」と「タイトル」を表示するためのViewクラスです。
//  TileViewControllerでの利用を想定しています。
// 
//  Created by 孝文 田村 on 12/07/22.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import "WAYTileCellView.h"
#import "WAYImageUtil.h"

#import "UIImageView+AFNetworking.h"

@implementation WAYTileCellView

// TileCellのサイズ初期値
static float titleHeight_ =  20.0;  // TileCellの下のタイトル部分の高さ



- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

const float paddingSize_ = 5;
-(id)initWithFrame:(CGRect)rect image:(UIImage *)image title:(NSString *)title
{
    self = [[WAYTileCellView alloc]initWithFrame:rect];
    
    UIImage *i = [WAYImageUtil clipImage:image size:rect.size];
    i = [WAYImageUtil image:i withTitle:title width:titleHeight_];
    image_ = [WAYImageUtil shadowWithImage:i width:5.0];
    
    return self;
    
}


// TODO private化
// TODO onclickイベントでのコールバックの設定
// TODO (id)を受け取る? UIViewそのものに「名前」の枠があればそれを使う。



- (void)drawRect:(CGRect)rect
{
    [image_ drawInRect: rect];
}
@end
