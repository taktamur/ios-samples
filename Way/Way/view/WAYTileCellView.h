//
//  TileCellVIew.h
//  exp_TileView
//
//  タイル状の「画像」と「タイトル」を表示するためのViewクラスです。
//  TileViewControllerでの利用を想定しています。
//
//  Created by 孝文 田村 on 12/07/22.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WAYTileCellView : UIView
{
@private
    UIImage *image_;
}

-(id)initWithFrame:(CGRect)rect image:(UIImage *)image title:(NSString *)title;



// ファクトリーメソッド
//-(id)tileCellWithImage:(UIImage *)image title:(NSString *)title;

@end
