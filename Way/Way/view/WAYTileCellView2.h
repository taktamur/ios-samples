//
//  WAYTileCellView2.h
//  Way
//
//  Created by takafumi tamura on 12/08/05.
//  Copyright (c) 2012年 Takafumi Tamura. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WAYTileCellView2 : UIImageView

-(void)setImageURL:(NSURL *)url;
-(void)setTouched:(void (^)(id sender))touched;
@property (nonatomic,strong)NSString *uid;
@end
