//
//  PAMViewController.m
//  exp_TileView
//
//  Created by 孝文 田村 on 12/07/22.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <CoreLocation/CoreLocation.h>

#import "WAYTileViewController.h"
#import "WAYTileMatrix.h"
#import "WAYYolpLocalSearch.h"
#import "WAYTileCellView2.h"
#import "WAYDetailViewController.h"
// AFNetwork
#import "AFJSONRequestOperation.h"

@interface WAYTileViewController()
@property (nonatomic,strong)WAYTileMatrix *matrix;
@property double tileWidth;
@property (nonatomic,strong)CLLocationManager *locationManager;
@property CLLocationCoordinate2D coordinate;
@end

@implementation WAYTileViewController
// 公開プロパティ
@synthesize scrollView = _scrollView;
@synthesize mapView = _mapView;
// 非公開プロパティ
@synthesize matrix = _matrix;
@synthesize tileWidth = _tileWidth;
@synthesize locationManager = _locationManager;
@synthesize coordinate = _coordinate;

const int _columNum = 3;

#pragma mark -
#pragma mark Viewのライフサイクル
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

// NavigationBarの写真地図切り替え時に
// 呼び出されるメソッド
- (void)segmentDidChange:(id)sender
{
    if ([sender isKindOfClass:[UISegmentedControl class]]) {
        // subviewの切り替え
        UISegmentedControl *segment = sender;
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromLeft
                               forView:self.view
                                 cache:YES];
        [UIView setAnimationDuration:0.5];
        if (segment.selectedSegmentIndex == 0) {
            self.scrollView.hidden=NO;
            self.mapView.hidden=YES;
        }else{
            self.scrollView.hidden=YES;
            self.mapView.hidden=NO;
        }
        [UIView commitAnimations];
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
//    self.scrollView = [[UIScrollView alloc]initWithFrame:self.view.frame];
    // 縦スクロール可能にしておく。
//    self.scrollView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
//    [self.view addSubview:self.scrollView];

    // NavigationBarへの登録
    self.navigationItem.title=@"Wayモック";
    UIBarButtonItem *reloadButton = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemRefresh target:self action: @selector(reloadYOLPFeature)];
    self.navigationItem.leftBarButtonItem = reloadButton;
    // 写真/地図の切り替えセグメント
    NSArray *segmentItems = [NSArray arrayWithObjects:@"写真",@"地図", nil];
    UISegmentedControl *segment = [[UISegmentedControl alloc]initWithItems:segmentItems];
    segment.selectedSegmentIndex=0;
    [segment addTarget:self
                action:@selector(segmentDidChange:)
      forControlEvents:UIControlEventValueChanged];
    UIBarButtonItem *barButton = [[UIBarButtonItem alloc]
                                  initWithCustomView:segment];
    self.navigationItem.rightBarButtonItem = barButton;
    
    // Wayのタイル表示用の準備
    self.tileWidth = self.view.frame.size.width / _columNum;
    _matrix = [[WAYTileMatrix alloc]initWithMaxX:_columNum];
    
    // mapViewを隠しておく
    self.mapView.hidden=YES;
//    self.mapView.delegate=self; // FIXME: Delegeteの準備ができたら復活。

    // LocationServiceを開始
    self.coordinate=CLLocationCoordinate2DMake(0,0); // 0,0を未処理状態として使う。
    if ([CLLocationManager locationServicesEnabled]) {
        self.locationManager = [[CLLocationManager alloc] init];
        self.locationManager.delegate = self;
        [self.locationManager startUpdatingLocation];
        NSLog(@"Start updating location.");
        
    } else {
        NSLog(@"The location services is disabled.");
    }
    
    self.view.backgroundColor=[UIColor whiteColor];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark -
#pragma mark UIから呼び出される部分
// reloadボタンを押されたときに呼び出され、
// self.coordinateを見て画面を更新する。
-(void) reloadYOLPFeature
{
    NSLog(@"reload");
    if ((self.coordinate.latitude != 0 )&&
        (self.coordinate.longitude != 0 )) {
        NSLog(@"update lat=%f lon=%f",self.coordinate.latitude, self.coordinate.longitude);
        [self updateYOLPFeatureWithCoordinate:self.coordinate];
    }
}

#pragma mark -
#pragma mark YOLPFeatureの更新と画面のupdate
- (void)addYOLPFeature:(WAYYolpFeature *)feature withLarge:(BOOL) isLarge
{
    // 画像を取得
    NSURL *imageurl = feature.leadImage;
    if(imageurl == nil) return;

    WAYTileCellView2 *v;
    if(isLarge){
        MatrixPoint p = [_matrix searchEmptyDoubleSpace];
        v = [[WAYTileCellView2 alloc]initWithFrame:CGRectMake(_tileWidth*p.x,_tileWidth*p.y,_tileWidth*2, _tileWidth*2)];
        [_matrix putDoubleTile:v toMatrixPoint:p];
    }else{
        MatrixPoint p = [_matrix searchEmptySingleSpace];
        v = [[WAYTileCellView2 alloc]initWithFrame:CGRectMake(_tileWidth*p.x,_tileWidth*p.y,_tileWidth, _tileWidth)];
        [_matrix putSingleTile:v toMatrixPoint:p];
    }
    [v setImageURL:imageurl];

    // block部分用
    // touch時の処理でNavigationControllとuidを使うので、
    // 束縛できるように変数に入れておく。
    UINavigationController *navigationController = self.navigationController;
    NSString *uid = feature.uid;
    
    // タッチされたときの処理を追加
    [v setTouched:^(id sender){
        NSLog(@"touched. uid=%@",uid);
        // UIDからURLを組み立ててロコ画面に飛ばしておく。
        NSString *url = [NSString stringWithFormat:@"http://loco.yahoo.co.jp/place/%@",uid];
        WAYDetailViewController *c = [[WAYDetailViewController alloc]
                                      initWithNibName:@"WAYDetailViewController" bundle:nil];
        c.url = url;
        [navigationController pushViewController:c animated:YES];
    }];
    
    // ScrollViewのサイズ再調整
    [self.scrollView addSubview:v];
    self.scrollView.contentSize = CGSizeMake(_tileWidth*_columNum, _tileWidth * [_matrix countYSize]);
    
    // ここからは地図へのピン立て
    [self.mapView addAnnotation:feature];
//    CGRect rect = mapView.annotationVisibleRect;
//    [self.mapView setVisibleMapRect:rect animated:YES];
}

-(void)updateYOLPFeatureWithCoordinate:(CLLocationCoordinate2D) c
{
    
    // YOLPからPOIを取得
    WAYYolpLocalSearch *localSearch = [[WAYYolpLocalSearch alloc] initWithAppID:@"iqepABuxg66Ex7hDBiYGDXZQkg_NSSY.EsBVsKX27f3_Sd2814W4gy_QmARcGg--" ];
    localSearch.resultNum = 30;
    localSearch.distKm=1;
    localSearch.detail = YOLPLocalSearchDetailTypeStandard;
    localSearch.isImage=YES;
    
    NSURL *url = [localSearch urlWithCoordinate:c];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];

    // ステータスバーの通信アイコンをまわす
    UIApplication *app = [UIApplication sharedApplication];
    app.networkActivityIndicatorVisible=YES;
    
    AFJSONRequestOperation *operation =
    [AFJSONRequestOperation
     JSONRequestOperationWithRequest:request
     success:^(NSURLRequest *request,
               NSHTTPURLResponse *response,
               id JSON) {
         // すでにあるViewなどをクリアにする
         [self.matrix clear];
         for (UIView *v in self.scrollView.subviews) {
             [v removeFromSuperview];
         };
         // ステータスバーの通信アイコンを止める
         UIApplication *app = [UIApplication sharedApplication];
         app.networkActivityIndicatorVisible=NO;

         // 地図の移動、ピンの除去とヘディングアップ
         MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(c,1000,1000);
         [self.mapView setRegion:region animated:YES];
         NSArray *annotationArray = self.mapView.annotations;
         [self.mapView removeAnnotations:annotationArray];
         self.mapView.showsUserLocation=YES;
         [self.mapView setUserTrackingMode:MKUserTrackingModeFollowWithHeading animated:YES];

         // YOLPFeatureをViewに追加していく。
         NSArray *ary = [JSON objectForKey:@"Feature"];
         for( int count=0; count<[ary count]; count++){
             NSDictionary *dic = [ary objectAtIndex:count];
             WAYYolpFeature *poi = [WAYYolpFeature featureFromKVCObject:dic];
             if( count%4 == 3){
                 [self addYOLPFeature:poi withLarge:NO];
             }else{
                 [self addYOLPFeature:poi withLarge:NO];
             }
         }
     }
     failure:nil];
    
    [operation start];
}




#pragma mark -
#pragma mark locationManagerから呼び出される部分

- (void)locationManager:(CLLocationManager *)manager
    didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation {
    NSLog(@"location update.lat=%f,log=%f",newLocation.coordinate.latitude,
          newLocation.coordinate.longitude);
    if( (self.coordinate.latitude==0)&&
       (self.coordinate.longitude==0)){
        // 初回なので、画面を更新する。
        [self updateYOLPFeatureWithCoordinate:newLocation.coordinate];
    }
    self.coordinate = newLocation.coordinate; // ここで値が入れば、他メソッドでの場所処理が走る。
    if( ( self.coordinate.latitude == 37.785834 )&&
       (self.coordinate.longitude == -122.406417 ) ){
        NSLog(@"simulater? skip.");
        self.coordinate = CLLocationCoordinate2DMake(35.5645149450811, 139.65457229636968);
    }

}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error{
    NSLog(@"Error: %@", error);
}

#pragma mark -
#pragma mark MKMapViewDelegate
/*
- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id < MKAnnotation >)annotation
{
    MKAnnotationView *view = [[MKAnnotationView alloc]initWithAnnotation:annotation reuseIdentifier:nil];
    // FIXME: 画像をはめ込みたい。
    return view;
}
*/
@end
