//
//  WAYDetailViewController.m
//  Way
//
//  Created by takafumi tamura on 12/08/05.
//  Copyright (c) 2012年 Takafumi Tamura. All rights reserved.
//

#import "WAYDetailViewController.h"

@interface WAYDetailViewController ()

@end

@implementation WAYDetailViewController
@synthesize webView = _webView;
@synthesize url = _url;
#pragma mark -
#pragma mark UIView ライフサイクル
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.webView.delegate = self;

}
-(void)viewWillAppear:(BOOL)animated
{
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:self.url]];
    [self.webView loadRequest:request];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
}
- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark -
#pragma mark UIWebViewDelegate
- (void)webViewDidStartLoad:(UIWebView *)webView
{
    // 通信中の表示
    UIApplication *app = [UIApplication sharedApplication];
    app.networkActivityIndicatorVisible=YES;
    // NagivationBarにurl表示
    self.navigationItem.title = [self.webView.request.URL description];
}
- (void)webViewDidFinishLoad:(UIWebView *)webView{
    // 通信中の表示停止
    UIApplication *app = [UIApplication sharedApplication];
    app.networkActivityIndicatorVisible=NO;
}
@end
