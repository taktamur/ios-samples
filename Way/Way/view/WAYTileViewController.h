//
//  PAMViewController.h
//  exp_TileView
//
//  Created by 孝文 田村 on 12/07/22.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import <MapKit/MapKit.h>
@interface WAYTileViewController : UIViewController
                        <CLLocationManagerDelegate,MKMapViewDelegate>
@property (nonatomic,assign)IBOutlet UIScrollView *scrollView;
@property (nonatomic,assign)IBOutlet MKMapView *mapView;

#pragma mark -
#pragma mark MKMapViewDelegate
//- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id < MKAnnotation >)annotation;
@end
