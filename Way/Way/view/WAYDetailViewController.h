//
//  WAYDetailViewController.h
//  Way
//
//  Created by takafumi tamura on 12/08/05.
//  Copyright (c) 2012年 Takafumi Tamura. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WAYDetailViewController : UIViewController<UIWebViewDelegate>
@property (nonatomic,assign) IBOutlet UIWebView* webView;
@property (nonatomic,strong) NSString *url;

- (void)webViewDidFinishLoad:(UIWebView *)webView;
- (void)webViewDidStartLoad:(UIWebView *)webView;
@end
