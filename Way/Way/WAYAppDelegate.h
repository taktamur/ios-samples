//
//  AppDelegate.h
//  Way
//
//  Created by takafumi tamura on 12/08/05.
//  Copyright (c) 2012年 Takafumi Tamura. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WAYAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
