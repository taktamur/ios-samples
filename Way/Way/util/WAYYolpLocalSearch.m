//
//  YOLPLocalSearch.m
//  NearMap
//
//  Created by 田村 孝文 on 12/07/08.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "WAYYolpLocalSearch.h"
//#import "ASyncURLConnection.h"

@interface WAYYolpLocalSearch()
@property (nonatomic)NSString *appid;
@end

@implementation WAYYolpLocalSearch
@synthesize appid = _appid;
@synthesize distKm=_distKm;
@synthesize resultNum=_resultNum;
@synthesize detail=_detail;
@synthesize isImage = _isImage;


-(id)initWithAppID:(NSString *)appid
{
    if( (self = [super init])!=nil){
        _appid = appid;
        _resultNum = 10;
        _distKm=10;
        _detail = YOLPLocalSearchDetailTypeStandard;
        _isImage=NO;
    };
    return self;
}



-(NSURL *)urlWithCoordinate:(CLLocationCoordinate2D) c
{
    NSString *detail;
    switch (_detail){
        case YOLPLocalSearchDetailTypeFull:
            detail = @"full";
            break;
        case YOLPLocalSearchDetailTypeStandard:
            detail=@"standard";
            break;
        default:
            detail = @"simple";
    }
    NSString *image= @"false";
    if(_isImage) image=@"true";
    NSString *url = [NSString stringWithFormat:@"http://search.olp.yahooapis.jp/OpenLocalPlatform/V1/localSearch?appid=%@&lat=%f&lon=%f&results=%d&output=json&detail=%@&sort=hybrid&dist=%d&image=%@",
                     _appid, c.latitude, c.longitude, _resultNum,detail,_distKm,image];
    return [NSURL URLWithString:url];
}

-(NSData *)dataFromURL:(NSURL *)url
{
    NSURLRequest * request = [NSURLRequest requestWithURL:url];
    NSData * response = [NSURLConnection sendSynchronousRequest:request
                                              returningResponse:nil
                                                          error:nil];
    return response;
}


// YDFのjsonデータをパースして、YOLPFeatureオブジェクトのArrayとして返却する。
// このメソッドは、いっさいメンバ変数を使っていないので、クラス関数とした。
+(NSArray *)arrayFromJSONData:(NSData *)data
{
    NSDictionary *ydfJsonDic = [NSJSONSerialization JSONObjectWithData:data 
                                                        options:NSJSONWritingPrettyPrinted 
                                                          error:nil];
    NSMutableArray *featuresArray = [NSMutableArray array];
    for( NSDictionary *feature in [ydfJsonDic objectForKey:@"Feature"]){
//        [featuresArray addObject:[YOLPFeature featureFromKVCObject:feature]];
        [featuresArray addObject:[[WAYYolpFeature alloc]initWithKVCObject:feature]];
    }
    return featuresArray;
}

/*
-(void)asyncLocalSearchWithCoordinate:(CLLocationCoordinate2D) coordinate
                             complete: (POIArrayBlock)completeBlock
                                error:(ErrorBlock)errorBlock;
{
    NSURL *url = [self urlWithCoordinate:coordinate];
    [AsyncURLConnection request:url completeBlock:^(NSData *data){
        dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT,0); 
        dispatch_async(queue, ^{
            completeBlock( [YOLPLocalSearch arrayFromJSONData:data]);
        });
    } errorBlock:^(NSError *e){
        NSLog(@"error %@",e);
        errorBlock(e);
    }];
}
*/

@end
