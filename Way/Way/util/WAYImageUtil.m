//
//  WAYImageUtil.m
//  WAYUtil
//
//  Created by takafumi tamura on 12/07/27.
//  Copyright (c) 2012年 Takafumi Tamura. All rights reserved.
//
    
#import "WAYImageUtil.h"

@implementation WAYImageUtil

// http://blog.livedoor.jp/arumisoft/archives/6688207.html
+(UIImage *) clipImage:(UIImage *)original size:(CGSize)size
{

    // リサイズ画像のx,y,width,heightを算出
    float resized_x = 0.0;
    float resized_y = 0.0;
    float resized_width = size.width;
    float resized_height = size.height;

    // 縦横それぞれの「倍率」を算出し、より大きな倍率（＝大きな画像）を採用する。
    float ratio_width = size.width/original.size.width;
    float ratio_height = size.height/original.size.height;
    if( ratio_width < ratio_height ){
        // 縦の倍率採用
        resized_width = original.size.width*ratio_height;
        resized_x = (size.width-resized_width)/2; // 横ははみ出る
    }else{
        // 横の倍率採用
        resized_height = original.size.height*ratio_width;
        resized_y = (size.height-resized_height)/2; // 縦ははみ出る
    }
    
    // リサイズとクリップ処理
    CGSize resized_size = CGSizeMake(size.width ,size.height);
    UIGraphicsBeginImageContext(resized_size);
    // はみ出してる部分を切り落とす
    [original drawInRect:CGRectMake(resized_x, resized_y, resized_width, resized_height)];
    UIImage* resized_image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return resized_image;
}

+(UIImage *) image:(UIImage *)original withTitle:(NSString *)title width:(float)titile_width
{
    UIGraphicsBeginImageContext(original.size);
    [original drawInRect:CGRectMake(0,0,original.size.width,original.size.height)];
    CGContextRef context = UIGraphicsGetCurrentContext();
        
    CGRect titleRect = CGRectMake(0, original.size.height-titile_width, original.size.width, original.size.height);
    CGContextSetRGBFillColor(context, 1.0, 1.0, 1.0, 1.0);
    CGContextFillRect(context, titleRect);
    
    // 文字列を描画
    CGContextSetFillColorWithColor(context, [[UIColor blackColor] CGColor]);
    [title drawInRect: titleRect withFont:[UIFont systemFontOfSize:18]];

    // 現在のコンテキストのビットマップをUIImageとして取得
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    // コンテキストを閉じる
    UIGraphicsEndImageContext();
    return image;
}

+(UIImage *)shadowWithImage:(UIImage *)origin width:(float)f
{
    UIGraphicsBeginImageContext(CGSizeMake(origin.size.width + f, origin.size.height + f));
    CGContextSetShadow(UIGraphicsGetCurrentContext(), CGSizeMake(f/2, f/2), 10.0f);
    [origin drawAtPoint:CGPointZero];
    UIImage *result = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return result;
}
@end
