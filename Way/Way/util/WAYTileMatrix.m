//
//  TileMatrix.m
//  exp_TileView
//
//  Created by 孝文 田村 on 12/07/22.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "WAYTileMatrix.h"
#import <Foundation/Foundation.h>

MatrixPoint MatrixPointMake(unsigned int x,unsigned int y){
    MatrixPoint p;
    p.x = x;
    p.y = y;
    return p;
}


@implementation WAYTileMatrix

// xは横方向、yは縦方向のマトリクスの数。左上を0,0とする。
// (y,x) 
// (0,0) (0,1) (0,2)
// (1,0) (1,1) (1,2)

-(id)initWithMaxX:(unsigned int)x
{
    self = [super init];
    if (self) {
        max_x = x;
        matrix_y = [NSMutableArray array];
    }
    return self;
}

-(BOOL)isEmptyPoint:(MatrixPoint)p
{
    id o = [self objectFromMatrixPoint:p];
    if( (o == [NSNull null] )||
       (o == nil )){
        return YES;
    }else{
        return NO;
    }
}

// Matrixから１マス開いているpointを探す
-(MatrixPoint)searchEmptySingleSpace
{
    MatrixPoint p;
    for(int y=0; y<=[matrix_y count]; y++ ){
        for( int x=0; x<max_x; x++ ){
            p = MatrixPointMake(x, y);
            if( [self isEmptyPoint:p] ){
                // 開いてたら、その場所を返す
                return p;
            }
        }
    }
    return p;
}



// Matrixから2マス四方が開いているpoint（左上)を探す
-(MatrixPoint)searchEmptyDoubleSpace
{
    for(int y=0; y<=([matrix_y count]); y++ ){
        for( int x=0; x<(max_x-1); x++ ){
            MatrixPoint p = MatrixPointMake(x, y);
            // 2マス四方なので前後をチェックしている。
            if(   [self isEmptyPoint:MatrixPointMake(x  ,y  ) ]
               && [self isEmptyPoint:MatrixPointMake(x+1,y  ) ]
               && [self isEmptyPoint:MatrixPointMake(x,  y+1) ]
               && [self isEmptyPoint:MatrixPointMake(x+1,y+1) ] ){
                // 開いてたら、その場所を返す
                return p;
            }
        }
    }
    MatrixPoint p = MatrixPointMake(-1,-1);
    return p;    
}

-(id)objectFromMatrixPoint:(MatrixPoint)p 
{
    if( [matrix_y count] <= p.y ) return nil;
    NSMutableArray *ary_x = [matrix_y objectAtIndex:p.y];
    if( [ary_x count] <= p.x) return nil;
    id ret = [ary_x objectAtIndex:p.x];
    if( ret == [NSNull null] ) ret = nil;
    return ret;
}
-(void) putSingleTile:(id)object toMatrixPoint:(MatrixPoint)p
{
    if( [matrix_y count] <= p.y ){
        // matrix_yの長さ（縦幅）が足らないので追加していく。
        for( int i=[matrix_y count]; i<=p.y; i++ ){
            NSMutableArray *ary = [NSMutableArray array];
            // ary(横一列を表す)にはNullオブジェクトを登録
            for( int j=0; j<max_x; j++ ){
                [ary addObject:[NSNull null]];
            }
            [matrix_y addObject:ary];
        }
    }
    NSMutableArray *array_x = [matrix_y objectAtIndex:p.y];
    [array_x replaceObjectAtIndex:p.x withObject:object];
}    
-(void) putDoubleTile:(id)object toMatrixPoint:(MatrixPoint)p
{
    [self putSingleTile:object toMatrixPoint:MatrixPointMake(p.x  , p.y  )];
    [self putSingleTile:object toMatrixPoint:MatrixPointMake(p.x+1, p.y  )];
    [self putSingleTile:object toMatrixPoint:MatrixPointMake(p.x  , p.y+1)];
    [self putSingleTile:object toMatrixPoint:MatrixPointMake(p.x+1, p.y+1)];
}
-(int) countYSize
{
    return [matrix_y count];
}

-(void)clear{
    [matrix_y removeAllObjects];
}


-(NSString *)description
{
    NSMutableString *desctiption = [NSMutableString stringWithCapacity:0];
    for (NSArray *array_x in matrix_y) {
        [desctiption appendString:@"("];
        for( int x=0; x<[array_x count]; x++ ){
            if( [array_x objectAtIndex:x] == nil){
                [desctiption appendString:@" ,"];
            }else if( [array_x objectAtIndex:x] == [NSNull null]){
                [desctiption appendString:@" ,"];
            }else{
                [desctiption appendString:@"o,"];
            }
        }
        [desctiption appendString:@")"];
    }
    return desctiption;
}
@end
