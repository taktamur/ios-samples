//
//  POI.h
//  NearMap
//
//  Created by 田村 孝文 on 12/07/08.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import <Foundation/NSKeyValueCoding.h>
#import <MapKit/MapKit.h>

//YDFフォーマットのFeatureを表すクラス
// [YOLPLocalSearch のarrayFromDataで生成される。

@interface WAYYolpFeature : NSObject <MKAnnotation>
// Feature[n].Name
@property (readonly) NSString *name;
@property (readonly) NSString *title;
// Feature[n].Geometory.Coordinate
@property (nonatomic,readonly) CLLocationCoordinate2D coordinate;
// Feature[n].Property.Uid
@property(readonly,getter=getUid) NSString *uid;
// Feature[n].Property.LeadImage
@property (readonly,getter = getLeadImage) NSURL *leadImage;
// Feature[n].Property.Detail.pcUrl
@property (readonly,getter=getPcURL) NSURL *pcUrl;
// Featuer[n].Property.Detail.Image[x]
// 注意：YDFでは1始まりだが、このimagesは0始まりのArrayとなっている。
@property (readonly,getter=getImagesURLArray) NSArray *images;
// Feature[n].Property.Detail.YUrl
@property (readonly,getter=getYURL) NSURL *yUrl;

+(WAYYolpFeature *)featureFromKVCObject:(id)obj;

-(id)initWithKVCObject:(NSObject *)obj;

@end
