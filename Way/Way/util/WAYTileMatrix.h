//
//  TileMatrix.h
//  exp_TileView
//
//  Created by 孝文 田村 on 12/07/22.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef struct{
    unsigned int x;
    unsigned int y;
}MatrixPoint;

MatrixPoint MatrixPointMake(unsigned int x,unsigned int y);

@interface WAYTileMatrix : NSObject
{
@private
    unsigned int max_x;
    unsigned int max_y;
    NSMutableArray *matrix_y;
    
}
-(id)initWithMaxX:(unsigned int)y;
-(MatrixPoint)searchEmptySingleSpace;
-(MatrixPoint)searchEmptyDoubleSpace;  // TODO: SingleとDoubleは１つにできそう。
//-(id)objectFromMatrixPoint:(MatrixPoint)p;
-(void) putSingleTile:(id)object toMatrixPoint:(MatrixPoint)p;
-(void) putDoubleTile:(id)object toMatrixPoint:(MatrixPoint)p;
-(int) countYSize;
-(id)objectFromMatrixPoint:(MatrixPoint)p;
-(void)clear;
@end
