//
//  POI.m
//  NearMap
//
//  Created by 田村 孝文 on 12/07/08.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "WAYYolpFeature.h"

@interface WAYYolpFeature()
@property (nonatomic,strong) NSObject *kvcObj;
@end


@implementation WAYYolpFeature

@synthesize kvcObj=_kvcObj;

- (id)init
{
    self = [super init];
    if (self) {
    }
    return self;
}

-(id)initWithKVCObject:(NSObject *)obj
{
    if( (self = [super init])!=nil ){
        _kvcObj = obj;
    }
    return self;
}

-(NSString *)name
{
    return [_kvcObj  valueForKeyPath:@"Name"];
}
-(NSString *)title
{
    return self.name;
}
-(NSString *)getUid
{
    return [_kvcObj valueForKeyPath:@"Property.Uid"];
}

-(NSURL *)getLeadImage
{
    return [NSURL URLWithString:[_kvcObj valueForKeyPath:@"Property.LeadImage"]];
}

-(NSURL *)getPcURL
{
    return [NSURL URLWithString:[_kvcObj valueForKeyPath:@"Property.Detail.PcUrl1"]];
}

-(NSURL *)getYURL
{
    return [NSURL URLWithString:[_kvcObj valueForKeyPath:@"Property.Detail.YUrl"]];
}

-(CLLocationCoordinate2D)coordinate
{
    NSArray *tmp = [[_kvcObj valueForKeyPath:@"Geometry.Coordinates"] componentsSeparatedByString:@","];
    double lat =  [(NSString *)[tmp objectAtIndex:1] doubleValue];
    double lon = [(NSString *)[tmp objectAtIndex:0] doubleValue];
    return CLLocationCoordinate2DMake(lat,lon);
}

-(NSArray *)getImagesURLArray
{
    // Property.Detail.image[1~10]があれば、取得する
    NSMutableArray *imageURLArray = nil;
    for(int i=1; i<=10; i++){ // 10個まで取得を試みる。
        NSString *path = [NSString stringWithFormat:@"Property.Detail.Image%d",i];
        NSString *imgurlStr = [_kvcObj valueForKeyPath:path];
        if( imgurlStr == nil ) break; // 空になったらそこでアボート
        if( imageURLArray == nil )imageURLArray = [[NSMutableArray alloc]init];
        [imageURLArray addObject:[NSURL URLWithString:imgurlStr]];
    }
    return imageURLArray;
}
// KVCのオブジェクトから、YDFフォーマットのFeature以下が来てると期待して
// YOLPFeatureを組み立てて返す
+(WAYYolpFeature *)featureFromKVCObject:(id)obj{
    WAYYolpFeature *feature = [[WAYYolpFeature alloc]initWithKVCObject:obj];
    return feature;  
}

@end
