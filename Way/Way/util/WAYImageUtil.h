//
//  WAYImageUtil.h
//  WAYUtil
//
//  Created by takafumi tamura on 12/07/27.
//  Copyright (c) 2012年 Takafumi Tamura. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

// Wayで使う、Image系のユーティリティ関数の集合です。
@interface WAYImageUtil : NSObject

// 画像を指定サイズいっぱいに切り取る
+(UIImage *) clipImage:(UIImage *)original size:(CGSize)size;

// 指定画像に影を付与する
+(UIImage *) shadowWithImage:(UIImage *)origin width:(float)f;

// 指定画像の下に、文字（タイトル）を追加する。
+(UIImage *) image:(UIImage *)original withTitle:(NSString *)title width:(float)titile_width;

@end
