//
//  YOLPLocalSearch.h
//  NearMap
//
//  Created by 田村 孝文 on 12/07/08.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WAYYolpFeature.h"
#import <CoreLocation/CoreLocation.h>

typedef void (^POIArrayBlock)(NSArray *ary);
typedef void (^ErrorBlock)(NSError *error);

// Yahoo! ローカルサーチ APIで検索するクラスです。
//  http://developer.yahoo.co.jp/webapi/map/openlocalplatform/v1/localsearch.html
// 主に緯度経度から、その周辺の情報(POI)を検索します。
// 利用にはYJDNのappidが必要です。
//  http://help.yahoo.co.jp/help/jp/developer/developer-06.html

typedef enum {
    YOLPLocalSearchDetailTypeFull,
    YOLPLocalSearchDetailTypeStandard,
    YOLPLocalSearchDetailTypeSimple
} YOLPLocalSearchDetailType;

@interface WAYYolpLocalSearch : NSObject
@property int resultNum; // 取得する検索結果件数、デフォルト50
@property int distKm;    // 検索する半径 単位km デフォルト10km
@property YOLPLocalSearchDetailType detail;
@property BOOL isImage;


// 初期化
// appidが必要です。
-(id)initWithAppID:(NSString *)appid;

//
-(NSURL *)urlWithCoordinate:(CLLocationCoordinate2D) c;
-(NSData *)dataFromURL:(NSURL *)url;

// YDFのjsonデータをパースして、YOLPFeatureオブジェクトのArrayとして返却する。
// このメソッドはクラス変数です。
+(NSArray *)arrayFromJSONData:(NSData *)data;


// 非同期実行
/*
-(void)asyncLocalSearchWithCoordinate:(CLLocationCoordinate2D) coordinate
                      complete: (POIArrayBlock)completeBlock
                         error:(ErrorBlock)errorBlock;
*/
@end
