//
//  WAYUtilTests.m
//  WAYUtilTests
//
//  Created by takafumi tamura on 12/07/27.
//  Copyright (c) 2012年 Takafumi Tamura. All rights reserved.
//

#import "WAYUtilTests.h"
#import "WAYImageUtil.h"

@implementation WAYUtilTests

- (void)setUp
{
    [super setUp];
    
    // Set-up code here.
}

- (void)tearDown
{
    // Tear-down code here.
    
    [super tearDown];
}

- (void)testExample
{
//    STFail(@"Unit tests are not implemented yet in WAYUtilTests");
}
-(void)testClipImage
{
    NSString *path = [[NSBundle mainBundle]
                      pathForResource:@"89288_20322879" ofType:@"jpeg"];
    UIImage *i = [UIImage imageWithContentsOfFile:path];
    STAssertNotNil(i, @"画像のロードに失敗");
    NSLog(@"original size, width=%f height=%f",i.size.width,i.size.height);
    CGSize resize_size = CGSizeMake(100, 200);
    UIImage *small = [WAYImageUtil clipImage:i size:resize_size];
    NSLog(@"clip size, width=%f height=%f",small.size.width,small.size.height);
    STAssertEquals(small.size.width,  100.0f, @"縮小時の横幅がおかしい");
    STAssertEquals(small.size.height, 200.0f, @"縮小時の縦幅がおかしい");
    

}

@end
