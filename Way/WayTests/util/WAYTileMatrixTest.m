//
//  TileMatrixTest.m
//  WAYUtil
//
//  Created by takafumi tamura on 12/08/04.
//  Copyright (c) 2012年 Takafumi Tamura. All rights reserved.
//

#import "WAYTileMatrixTest.h"
#import "WAYTileMatrix.h"
@implementation WAYTileMatrixTest

-(void)testMatrix
{
    WAYTileMatrix *matrix = [[WAYTileMatrix alloc]initWithMaxX:3];
    STAssertEquals([matrix countYSize],0 , @"初期値がおかしい");
    NSLog(@"%@",matrix);
    
    STAssertNil([matrix objectFromMatrixPoint:MatrixPointMake(0,0)],
                @"空のはずなのにゴミが入ってる");
    STAssertNil([matrix objectFromMatrixPoint:MatrixPointMake(1,0)],
                @"空のはずなのにゴミが入ってる");
    STAssertNil([matrix objectFromMatrixPoint:MatrixPointMake(2,0)],
                @"空のはずなのにゴミが入ってる");
    STAssertNil([matrix objectFromMatrixPoint:MatrixPointMake(-1,0)],
                @"空のはずなのにゴミが入ってる");
    STAssertNil([matrix objectFromMatrixPoint:MatrixPointMake(3,0)],
                @"空のはずなのにゴミが入ってる");
    STAssertNil([matrix objectFromMatrixPoint:MatrixPointMake(0,1)],
                @"空のはずなのにゴミが入ってる");
    STAssertNil([matrix objectFromMatrixPoint:MatrixPointMake(0,2)],
                @"空のはずなのにゴミが入ってる");
    STAssertNil([matrix objectFromMatrixPoint:MatrixPointMake(0,3)],
                @"空のはずなのにゴミが入ってる");
    STAssertNil([matrix objectFromMatrixPoint:MatrixPointMake(0,-1)],
                   @"空のはずなのにゴミが入ってる");
    
    // 1個追加
    NSString *first = @"1st";
    MatrixPoint p = [matrix searchEmptySingleSpace];
    STAssertEquals(p.x, (unsigned int)0, @"(0,0)になってない");
    STAssertEquals(p.y, (unsigned int)0, @"(0,0)になってない");
    [matrix putSingleTile:first toMatrixPoint:p];
    NSLog(@"%@",matrix);

    STAssertNotNil([matrix objectFromMatrixPoint:MatrixPointMake(0,0)],
                @"オブジェクトが入っているはず");
    STAssertNil([matrix objectFromMatrixPoint:MatrixPointMake(1,0)],
                @"空のはず");
    STAssertNil([matrix objectFromMatrixPoint:MatrixPointMake(0,1)],
                @"空のはず");
    // Doubleサイズを２個追加
    NSString *second = @"seconds.";
    p = [matrix searchEmptyDoubleSpace];
    STAssertEquals(p.x, (unsigned int)1, @"(1,0)になってない");
    STAssertEquals(p.y, (unsigned int)0, @"(1,0)になってない");
    [matrix putDoubleTile:second toMatrixPoint:p];
    NSLog(@"%@",matrix);

    STAssertNotNil([matrix objectFromMatrixPoint:MatrixPointMake(1,0)],
                   @"オブジェクトが入っているはず");
    STAssertNotNil([matrix objectFromMatrixPoint:MatrixPointMake(2,0)],
                   @"オブジェクトが入っているはず");
    STAssertNotNil([matrix objectFromMatrixPoint:MatrixPointMake(1,1)],
                   @"オブジェクトが入っているはず");
    STAssertNotNil([matrix objectFromMatrixPoint:MatrixPointMake(2,1)],
                   @"オブジェクトが入っているはず");
    STAssertNil([matrix objectFromMatrixPoint:MatrixPointMake(0,1)],
                @"空のはず");
    p = [matrix searchEmptyDoubleSpace];
    STAssertEquals(p.x, (unsigned int)0, @"(2,0)になってない");
    STAssertEquals(p.y, (unsigned int)2, @"(2,0)になってない");
    [matrix putDoubleTile:second toMatrixPoint:p];
    NSLog(@"%@",matrix);
    STAssertNotNil([matrix objectFromMatrixPoint:MatrixPointMake(0,2)],
                   @"オブジェクトが入っているはず");
    STAssertNotNil([matrix objectFromMatrixPoint:MatrixPointMake(1,2)],
                   @"オブジェクトが入っているはず");
    STAssertNotNil([matrix objectFromMatrixPoint:MatrixPointMake(0,3)],
                   @"オブジェクトが入っているはず");
    STAssertNotNil([matrix objectFromMatrixPoint:MatrixPointMake(1,3)],
                   @"オブジェクトが入っているはず");
    STAssertNil([matrix objectFromMatrixPoint:MatrixPointMake(0,1)],
                @"空のはず");
    
    // singleを１個追加
    p = [matrix searchEmptySingleSpace];
    STAssertEquals(p.x, (unsigned int)0, @"(0,1)になってない");
    STAssertEquals(p.y, (unsigned int)1, @"(0,1)になってない");
    [matrix putSingleTile:first toMatrixPoint:p];
    NSLog(@"%@",matrix);
    
    // クリア
    [matrix clear];
    p = [matrix searchEmptySingleSpace];
    STAssertEquals(p.x, (unsigned int)0, @"(0,0)になってない");
    STAssertEquals(p.y, (unsigned int)0, @"(0,0)になってない");
    STAssertNil([matrix objectFromMatrixPoint:MatrixPointMake(0,0)],
                @"空のはずなのにゴミが入ってる");


}
@end
