//
//  YOLPTests.m
//  YOLPTests
//
//  Created by 田村 孝文 on 12/07/08.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "WAYYolpLocalSearchTest.h"
#import "WAYYolpLocalSearch.h"

@implementation WAYYolpLocalSearchTest

// このUnitTest用のAppID
NSString* appid_ = @"i77tRH2xg65djdzHpP2M8c2r2T.lVgKVW_r8Lvz_Yk_H6qpDKr__nMkWYB..nA--";

- (void)setUp
{
    [super setUp];
    // Set-up code here.
}

- (void)tearDown
{
    // Tear-down code here.
    
    [super tearDown];
}


// 同期型通信でのテスト
- (void)testSync
{
    WAYYolpLocalSearch *localSearch = [[WAYYolpLocalSearch alloc] initWithAppID:appid_];
    localSearch.resultNum=50;

    // テスト場所：元住吉
    CLLocationCoordinate2D loc = CLLocationCoordinate2DMake(35.5645149450811, 139.65457229636968);
    NSURL *url = [localSearch urlWithCoordinate:loc];
    NSLog(@"url=%@",url);
    NSData *d = [localSearch dataFromURL:url];
    STAssertNotNil(d,@"ローカルサーチとの通信に失敗");
    NSArray *ary =[WAYYolpLocalSearch arrayFromJSONData:d];
    STAssertNotNil(ary, @"Jsonのパースに失敗");
    NSLog(@"ary count=%d",[ary count]);
    // ローカルサーチは50件指定しても、gidによるまとめにより微妙に件数が減った状態でかえってくるため、
    // まぁそこそこかえってきてたらokとする。
    STAssertTrue(([ary count]>10),@"YOLP Localsearch Request failed. count under 10 " );
    STAssertTrue(([ary count]<=50),@"YOLP Localsearch Request failed. count upper 50 " );
    
}




// URLの組み立てのテスト
-(void)testUrlBuild
{
    WAYYolpLocalSearch *s = [[WAYYolpLocalSearch alloc]initWithAppID:@"appidHere"];
    s.resultNum = 55; // 55件取得するつもりで
    s.distKm = 33;    // 半径33kmのつもりで。
    CLLocationCoordinate2D loc = CLLocationCoordinate2DMake(100.0,200.0); //緯度経度はわかりやすく。
    NSURL *url = [s urlWithCoordinate:loc];
    NSString *successString = @"http://search.olp.yahooapis.jp/OpenLocalPlatform/V1/localSearch?appid=appidHere&lat=100.000000&lon=200.000000&results=55&output=json&detail=standard&sort=hybrid&dist=33&image=false";
    STAssertEqualObjects([url description], successString, @"URLがおかしい(detail=standard)");
    s.detail = YOLPLocalSearchDetailTypeFull;
    url = [s urlWithCoordinate:loc];
    successString = @"http://search.olp.yahooapis.jp/OpenLocalPlatform/V1/localSearch?appid=appidHere&lat=100.000000&lon=200.000000&results=55&output=json&detail=full&sort=hybrid&dist=33&image=false";
    STAssertEqualObjects([url description], successString, @"URLがおかしい(detail=full)");

    s.detail = YOLPLocalSearchDetailTypeSimple;
    url = [s urlWithCoordinate:loc];
    successString = @"http://search.olp.yahooapis.jp/OpenLocalPlatform/V1/localSearch?appid=appidHere&lat=100.000000&lon=200.000000&results=55&output=json&detail=simple&sort=hybrid&dist=33&image=false";
    STAssertEqualObjects([url description], successString, @"URLがおかしい(detail=simple)");
    
                          
}


-(void)testStandardJsonParse
{

    NSString *path = [[NSBundle mainBundle]
                      pathForResource:@"yolp_standard_10" ofType:@"json"];
    NSData *d = [NSData dataWithContentsOfFile:path];
    NSArray *array = [WAYYolpLocalSearch arrayFromJSONData:d];
    STAssertNotNil(array, @"jsonのパースエラー");
    STAssertEquals([array count], (unsigned int)10, @"検索結果件数が合わない. count=%@",[array count]);

    // 先頭１個目のfeatureでテスト
    WAYYolpFeature *feature = [array objectAtIndex:0];
    STAssertEqualObjects(feature.name, @"Ash 元住吉店", @"店名がおかしい");

    // 緯度経度 精度後さがあるからAccuracyでチェック
    STAssertEqualsWithAccuracy(feature.coordinate.latitude, 35.5651506,  0.00001, @"緯度経度がおかしい。");
    STAssertEqualsWithAccuracy(feature.coordinate.longitude, 139.654096, 0.00001, @"緯度経度がおかしい。");

    STAssertEqualObjects([feature.leadImage description] ,
                         @"http://tenpo-t000116386-release.octfs.jp/loco_image",
                         @"leadImageURLがおかしい");

    // URL関連(Feature[x].Property.Detailは無いのでnilのはず)
    STAssertNil(feature.pcUrl, @"pcURLにゴミ");
    STAssertNil(feature.yUrl,  @"yURLにゴミ");
    STAssertNil(feature.images, @"imagesにゴミ");

}

// ローカルのjsonファイルを読み込み、ちゃんとパースしてオブジェクト生成できるかどうかのテスト
-(void) testFullYDFJsonParse
{
    NSString *path = [[NSBundle mainBundle] pathForResource: @"yolp_full_10" ofType: @"json" ];
    NSData *d = [NSData dataWithContentsOfFile: path];
    NSArray *array = [WAYYolpLocalSearch arrayFromJSONData:d];
    STAssertNotNil(array, @"jsonのパースエラー");
    STAssertEquals([array count], (unsigned int)10, @"検索結果件数が合わない. count=%@",[array count]);

    // 先頭１個目のfeatureでテスト
    WAYYolpFeature *feature = [array objectAtIndex:0];
    STAssertEqualObjects(feature.name, @"Ash 元住吉店", @"店名がおかしい");
    // URL関連
    STAssertEqualObjects([feature.pcUrl description],
                         @"http://locoplace.jp/t000116386/",
                         @"pcURLがおかしい");
    STAssertEqualObjects([feature.leadImage description] ,
                         @"http://tenpo-t000116386-release.octfs.jp/loco_image",
                         @"leadImageURLがおかしい");
    STAssertEqualObjects([feature.yUrl description],
                         @"http://loco.yahoo.co.jp/place/17e29de4dea154495b9b9dcb2b2b11e4dbd708d9/",
                         @"ロコのURLがおかしい。");
    // images関連
    NSArray *images = feature.images;
    STAssertEquals([images count], (unsigned int)6, @"imagesの個数がおかしい");
    NSURL *imgurl = [images objectAtIndex:0];
    STAssertEqualObjects([imgurl description],
                         @"http://tenpo-t000116386-release.octfs.jp/photo_image1",
                         @"image1のURLがおかしい");
    imgurl = [images objectAtIndex:5];
    STAssertEqualObjects([imgurl description],
                         @"http://tenpo-t000116386-release.octfs.jp/photo_image6",
                         @"image6のURLがおかしい");

    // 緯度経度 精度後さがあるからAccuracyでチェック
    STAssertEqualsWithAccuracy(feature.coordinate.latitude, 35.5651506,  0.00001, @"緯度経度がおかしい。");
    STAssertEqualsWithAccuracy(feature.coordinate.longitude, 139.654096, 0.00001, @"緯度経度がおかしい。");

    // 2個目の名前だけテスト
    feature = [array objectAtIndex:1];
    STAssertEqualObjects(@"ブランド楽市 元住吉店",feature.name, @"店名がおかしい");
}
@end
