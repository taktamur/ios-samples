//
//  WayTests.m
//  WayTests
//
//  Created by takafumi tamura on 12/08/05.
//  Copyright (c) 2012年 Takafumi Tamura. All rights reserved.
//

#import "WayTests.h"

@implementation WayTests

- (void)setUp
{
    [super setUp];
    
    // Set-up code here.
}

- (void)tearDown
{
    // Tear-down code here.
    
    [super tearDown];
}

- (void)nontestExample
{
    STFail(@"Unit tests are not implemented yet in WayTests");
}

@end
